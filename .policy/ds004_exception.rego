package builtin.dockerfile.DS004
	# this disables the rule DS004: Exposing port 22 might allow users to SSH into the container
	
	exception[rules] {
		rules := [""]
	}