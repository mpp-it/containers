# Containers

## Gitlab Runner

- container: `gitlab-registry.mpcdf.mpg.de/mpp-it/containers/gitlab_runner:latest`
- dockerfile: `gitlab_runner/Dockerfile`

Official alpine-based [GitLab CI runner container](https://hub.docker.com/r/gitlab/gitlab-runner/) with modified container-run to work with container engines in non-privileged mode with no extra subuid/subgid. This container is intended to be used as a shell runner. Other runner modes may not be supported. See [mpp-it/infrastructure](https://gitlab.mpcdf.mpg.de/mpp-it/infrastructure) for deployment guide using root-less podman.

## heaptrack

- container: `gitlab-registry.mpcdf.mpg.de/mpp-it/containers/heaptrack:latest`
- dockerfile: `heaptrack/Dockerfile`

heaptrack (a heap memory profiler for Linux) packaged inside a container. Read [this guide](heaptrack/README.md) for using it on clusters.

## Ubuntu sshd

- container: `gitlab-registry.mpcdf.mpg.de/mpp-it/containers/ubuntu_sshd:latest`
- dockerfile: `ubuntu_sshd/Dockerfile`
- ssh user:pass: `root:ubuntu`

Ubuntu container with sshd installed, ssh port exposed, and root login via password enabled. This container can be used for testing scripts.
